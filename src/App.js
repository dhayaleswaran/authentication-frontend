import React from 'react';
import { BrowserRouter , Route , Switch } from 'react-router-dom';
import { Login } from './Components/Login/login'; /* Login Component */
import { NotFound } from './Components/NotFound/notFound'; /* 404 Component */
import 'bootstrap/dist/css/bootstrap.min.css'; /* Bootstrap Stylesheet */
import { Register } from './Components/Register/register';
import { HomePage } from './Components/Home/home';

function App() {
  return (
      <React.StrictMode>
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={Login} />
          <Route path="/register" exact component={Register} />
          <Route path="/home" exact component={HomePage} />
          <Route path="/"  component={NotFound} />
        </Switch>
      </BrowserRouter>
      </React.StrictMode>
  );
}

export default App;
