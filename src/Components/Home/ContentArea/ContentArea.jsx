import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faSearch } from "@fortawesome/free-solid-svg-icons";
import "./ContentArea.scss";
import axios from "axios";

function ContentArea() {
  // Intial States
  const [data, setData] = useState({ isLoaded: false, userData: false });
  // For fetching UserData 
  useEffect(() => {
    axios.get("/users/home").then((response) => {
      setData({
        ...data,
        userData: response.data,
      });
    });
  }, []);

  // To check whether the data is loaded
  useEffect(() => {
    if(data.userData){
      setData({
        ...data,
        isLoaded : true
      })
    }
  },[data.userData])

  if(data.isLoaded){
    let tableData = [];
    for(let ele of data.userData){
      let { firstName , lastName , email } = ele;
      tableData.push(
        <tr key={email}>
          <td>{firstName}</td>
          <td>{lastName}</td>
          <td>{email}</td>
        </tr>
      )
    }

    return (
      <div className="col-sm-7 contentarea-container">
        <div className="row m-3">
          <div className="col-sm-6">
            <h2>Customers</h2>
          </div>
          <div className="col-sm-6">
            <div className="search-box-container d-flex w-100">
              <div className="plus-icon">
                <button className="btn btn-primary">
                  <FontAwesomeIcon icon={faPlus} />
                </button>
              </div>
              <div className="search-input input-group w-100">
                <div className="input-group-prepend additional-props-input-group-prepend">
                  <div className="input-group-text">
                    <FontAwesomeIcon icon={faSearch} />
                  </div>
                </div>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Search"
                />
              </div>
            </div>
          </div>
        </div>
        <div className="user-table mt-5">
          <table className="table table-hover">
            <thead>
              <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
              </tr>
            </thead>
            <tbody>
              {tableData}
            </tbody>
          </table>
        </div>
      </div>
    );
  } else {
    return(
      <div className="lds-dual-ring m-auto"></div>
    )
  }
} 

export { ContentArea };
