import React from 'react';
import './sideBar.scss';
import axios from 'axios';


function SideBar(){
    async function userLogout(){
        try {
            await axios.get("users/logout")
        } catch(err) {
            console.log(err)
        }
    }

    return(
        <div className="col-sm-2 sidebar-container">
            <ul className="sidebar-wrapper mt-5 pl-0">
                <li className="sidebar-item">Dashboard</li>
                <li className="sidebar-item">Users</li>
                <li className="sidebar-item">Manage Users</li>
                <li className="sidebar-item">Settings</li>
                <li className="sidebar-item" onClick={userLogout}>Log Out</li>
            </ul>
        </div>
    )
}

export default SideBar;