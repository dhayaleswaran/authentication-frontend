import SideBar from "./Sidebar/sideBar";
import { ContentArea } from "./ContentArea/ContentArea";
import { useEffect } from "react";

const HomePage = () => {
 
  return (
    <div className="container-fluid">
      <div className="row">
        <SideBar />
        <ContentArea />
      </div>
    </div>
  );
};

export { HomePage };
