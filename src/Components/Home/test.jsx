import React from 'react'
import { ContentArea } from './ContentArea/ContentArea';
import SideBar from './Sidebar/sideBar';
import axios from 'axios';

class HomePage extends React.Component{
  constructor(props){
    super();
    this.state = {
      data : null
    }
  }

  componentDidMount(){
      try {
         axios.get("/users/home").then(response => {
          this.setState({
            data : response.data
          })
        })
      } catch(err){
        console.log(err);
      }
  }

  render(){
    return(
      <div className="container-fluid">
        <div className="row">
          <SideBar />
          <ContentArea contentData={this.state.data}/>
        </div>
      </div>
    )
  }
}


export { HomePage }
