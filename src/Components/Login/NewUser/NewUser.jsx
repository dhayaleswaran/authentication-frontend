import React from "react";
import { Link } from "react-router-dom";
import "./NewUser.scss"; /* Stylesheet */

function NewUser() {
  return (
    <div className="newuser-container d-grid justify-content-center align-items-center p-3">
      <h2 className="mt-4 text-center">Create an Account <br /> Now!</h2> 
      <hr className="line-newuser mt-2" />
      <div className="text-center mt-4">
          <p>Creating an account is more easier and faster.Click below to create an account now</p>
          <Link to="/register"><button className="btn btn-newuser mt-4">Sign Up</button></Link>
      </div>
    </div>
  );
}

export { NewUser };
