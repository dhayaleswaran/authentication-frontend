import axios from "axios";
import iziToast from "izitoast";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import "./SignUp.scss"; /* Stylesheet for this component */
import "izitoast/dist/css/iziToast.min.css";

function SignUp() {
  const [formData, setFormData] = useState({
    email: "",
    password: "",
  });

  const changeHandler = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  const submitHandler = async (e) => {
    e.preventDefault();
    try {
      await axios.post("users/login", { formData });
    } catch (err) {
      if (!err) {
        iziToast.success({
          title: "Success",
          message: "Login Successful",
          position: "topCenter",
          timeout: "5000",
        });
      } else {
        iziToast.error({
          title: `${err}`,
          position: "topCenter",
          timeout: "5000",
        });
      }
    }
  };

  return (
    <div className="signup-container">
      <h2 className="text-primary mt-4 text-center">Sign in to Account</h2>
      <hr className="line-signup my-4" />
      <form onSubmit={submitHandler} className="col-sm-8 ml-auto mr-auto">
        <div className="form-group">
          <label htmlFor="Email">Email address</label>
          <input
            type="email"
            name="email"
            value={formData.email}
            onChange={changeHandler}
            className="form-control"
            required={true}
          />
          <span>{formData.email.msg}</span>
        </div>
        <div className="form-group">
          <label htmlFor="Password">Password</label>
          <input
            type="password"
            name="password"
            value={formData.password}
            onChange={changeHandler}
            className="form-control"
            required={true}
          />
        </div>
        <div className="text-center mt-4">
          <button type="submit" className="btn btn-block btn-primary">
            Sign In
          </button>
          <div className="text-center my-4">
            <Link to="#">Forgot Password</Link>
          </div>
        </div>
      </form>
    </div>
  );
}

export { SignUp };
