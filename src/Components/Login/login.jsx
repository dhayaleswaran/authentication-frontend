import React from "react";
import './login.scss';

import { NewUser } from "./NewUser/NewUser";
import { SignUp } from "./SignUp/SignUp";

function Login() {
  return (
    <div className="position-absolute h-100 w-100 d-flex align-items-center">
      <div className="container col-sm-5 shadow-lg additional-login-container">
        <div className="row">
            <div className="col-sm-8 additional-props-signup">
                <SignUp />
            </div>
            <div className="col-sm-4 additional-props-newuser bg-primary">
                <NewUser />
            </div>
        </div>
      </div>
    </div>
  );
}

export { Login };
