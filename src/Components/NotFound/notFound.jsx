import React from 'react';
import './notFound.scss'; /* Stylesheet */

function NotFound(){
    return(
        <div>
            <h1>Not Found</h1>
        </div>
    )
}

export { NotFound }