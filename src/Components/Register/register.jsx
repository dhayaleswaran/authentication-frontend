import axios from "axios";
import iziToast from "izitoast";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import "./register.scss";

const Register = () => {
  const [formData, setFormData] = useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
  });

  const changeHandler = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  const sumbitFormData = async (event) => {
    event.preventDefault();
    try {
      await axios.post("users/register", { formData }).then(resp => {
        iziToast.success({
          title : 'Success!!!',
          message : `${resp.data.msg}`,
          timeout : 6000,
          position : 'topCenter'
        })
      })
    } catch (err) {
      if (err) {
        console.log(err)
        return iziToast.error({
          title: `${err}`,
          timeout: 6000,
          position: "topCenter",
        });
      }
    }
  };

  return (
    <div className="register-container position-absolute align-items-center justify-content-center w-100 h-100 d-flex">
      <div className="registerform-container col-sm-3 p-4 ">
        <h2 className="text-center">Create an Account</h2>
        <form onSubmit={sumbitFormData}>
          <div className="form-group mt-4">
            <label htmlFor="first-name">First Name:</label>
            <input
              type="text"
              className="form-control"
              value={formData.firstName}
              name="firstName"
              onChange={changeHandler}
              required={true}
            />
          </div>
          <div className="form-group">
            <label htmlFor="last-name">Last Name:</label>
            <input
              type="text"
              className="form-control"
              value={formData.lastName}
              name="lastName"
              onChange={changeHandler}
              required={true}
            />
          </div>
          <div className="form-group">
            <label htmlFor="mail">Email:</label>
            <input
              type="email"
              className="form-control"
              value={formData.email}
              name="email"
              onChange={changeHandler}
              required={true}
            />
          </div>
          <div className="form-group">
            <label htmlFor="first-name">Password:</label>
            <input
              type="password"
              className="form-control"
              value={formData.password}
              name="password"
              onChange={changeHandler}
            />
          </div>
          <button type="submit" className="btn btn-success btn-block mt-4">
            Create an Account Now
          </button>
          <div className="mt-4 text-center">
            <Link to="/">Already have an Account</Link>
          </div>
        </form>
      </div>
    </div>
  );
};

export { Register };
